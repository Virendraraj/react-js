import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ViewExerciseComponent from './components/ViewExerciseComponent';
import ViewExerciseDetailsComponent from './components/ViewExerciseDetailsComponent';

function App() {
  return (
  <div>
      <Router>
              <div className="container-fluid">
                  <Switch> 
                        <Route path = "/" exact component = {ViewExerciseComponent}></Route>
                        <Route path = "/exercises/:id" component = {ViewExerciseComponent}></Route>
                        <Route path = "/view-exercise-details/:key" component = {ViewExerciseDetailsComponent}></Route>
                  </Switch>
              </div>
      </Router>
  </div>
  );
}

export default App;