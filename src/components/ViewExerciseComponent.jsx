import React, { Component } from 'react'
import ExerciseService from '../services/ExerciseService'
import * as Icon from 'react-bootstrap-icons';
import "../css/style.css";

var options = [];
const EXERCISE_API_BASE_URL = "http://64.225.12.22:3000";
var flag = false;
class ViewExerciseComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            exerciseDetails: [],
            results: []
        }
    }
    handleValidation(){
        let formIsValid = true;
        let fields = this.state.exerciseDetails;
        /*fields.map((subitem, i) => {
            if (!subitem.userAnswer) {
                formIsValid = false;
                subitem.error = "Cannot be empty";
            }
        });*/
        this.setState({exerciseDetails: fields});
       return formIsValid;
   }
    handleChange1(field, e){         
        let fields = this.state.exerciseDetails;
        // eslint-disable-next-line
        fields.map((subitem, i) => {
            if (i === field) {
                subitem.userAnswer = e.target.value
            }
        });   
        this.setState({exerciseDetails: fields});
    }
    handleBlur(field, e){         
        let fields = this.state.exerciseDetails;
        // eslint-disable-next-line
        fields.map((subitem, i) => {
            if (field === i) {
                
                if (subitem.userAnswer && e.target.value && e.target.value.replace(/[^a-zA-Z0-9 ]/g, '').trim() !== subitem.answer.replace(/[^a-zA-Z0-9 ]/g, '').trim()) {
                    subitem.error = "Answer is wrong";
                    subitem.success = "";
                } else if (subitem.userAnswer && e.target.value && e.target.value.replace(/[^a-zA-Z0-9 ]/g, '').trim()  === subitem.answer.replace(/[^a-zA-Z0-9 ]/g, '').trim()) {
                    subitem.success = "Answer is correct";
                    subitem.error = "";
                }
            }
        });
        this.setState({exerciseDetails: fields});
    }
    
    exerciseSubmit(event) {
        event.preventDefault();
        var answer = true;
        if(flag == false) {
            answer = window.confirm("Are you sure you want to submit?");
        }
        if (answer) {  

            let excerciseList = [];
            let excercise = {};
            let exercise_instance_id = 0;
            let fields = this.state.exerciseDetails;
            let isTrue = flag;
            // eslint-disable-next-line
            fields.map((subitem, i) => {
                let excerciseObject = {
                    exercise_detail_id: Number(subitem.exercise_detail_id),
                    exercise_id: Number(subitem.exercise_id),
                    question_key : subitem.key,
                    username: 'test',
                    exercise_instance_id: Number(subitem.exercise_instance_id),
                    random_gen_answer_id:subitem.random_gen_answer_id,
                    isAnswerCheck : flag
                }
                exercise_instance_id = Number(subitem.exercise_id);
                
                excerciseObject.details = [];

                for (var j = 0; j < (event.target.elements.length - 1); j++ ) {
                    if (event.target[j].offsetParent.offsetParent.offsetParent.id === subitem.key) {
                        let detailsObject = {};
                        detailsObject.answer_key = event.target[j].name;
                        detailsObject.submitted_answer = event.target[j].value;
                        excerciseObject.details.push(detailsObject);
                    }
                }
                excerciseList.push(excerciseObject);
            });
            excercise.Exercise = excerciseList;
            ExerciseService.saveDetails(exercise_instance_id, excercise).then( res => {
                this.setState({results: res.data.result});
                this.setState({journalEntry: res.data.journalEntry});
            })
        }
        /*if (this.handleValidation()) {
            console.log("success")
            let excerciseList = [];
            let excercise = {};
            let exercise_instance_id = 0;
            let fields = this.state.exerciseDetails;
            fields.map((subitem, i) => {
                let excerciseObject = {
                    username: 'test',
                    exercise_id: subitem.exercise_id,
                    exercise_instance_id: subitem.exercise_instance_id,
                    submitted_answer:  subitem.userAnswer,
                    question_key : subitem.key,
                    correct_answer: subitem.answer
                }
                exercise_instance_id = subitem.exercise_id;
                excerciseList.push(excerciseObject);
            });
            excercise.Exercise = excerciseList;
            ExerciseService.saveDetails(exercise_instance_id, excercise).then( res => {
                console.log(res.data)
                this.setState({results: res.data.result});
            })
        } else {
           return false;
        }*/
        return false;
    }

    componentDidMount(){
        ExerciseService.getDetailsById(this.state.id).then( res => {
            // eslint-disable-next-line
            res.data.data.map((subitem, i) => {
                subitem.userAnswer = "";
                subitem.error = "";
                subitem.success = "";
            });
            
            options = res.data.exerciseList.map(d => ({
                "value" : d.exercise_id,
                "label" : d.label
              }))
              
            this.setState({exerciseDetails: res.data.data});
            this.setState({exercises: options});
        })
    }
    handleChange(e){
        this.state.id = e.target.value;
        this.componentDidMount();
        window.history.pushState({}, null, EXERCISE_API_BASE_URL+"/exercises/"+this.state.id+"/details");
        this.state.results.length = 0;
       }
    getDetailsByKey(key){
        this.props.history.push(`/view-exercise-details/${key}`);
    }
    confirm() {
        flag = false;
    }
    checkAnswer() {
        flag = true;
    }
    render() { 
        return (
            <div>
                 <br/>
                <div className = "card col-md-12">
                    <div class="select-exr">
                        <h5 class="exercise-label">Select Exercise  </h5> 
                        <select class="col-md-3 select-container" value={this.state.id} onChange={this.handleChange.bind(this)}>
                            {options.map((option) => (
                            <option value={option.value}>{option.label}</option>
                            ))}
                        </select>
                    </div>
                    <br/>
                    <h3 className = "text-center page-title">Exercise Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <form name="exerciseform" className="exerciseform" onSubmit= {this.exerciseSubmit.bind(this)} autocomplete="off">
                                <table className = "table table-striped" border="1 ">
                                    <thead>
                                        <tr>
                                            <th class="detail_first_col"> Exercise Id</th>
                                            <th class="detail_second_col"> Detail Label</th>
                                            <th class="detail_third_col"> Detail Key</th>
                                            <th class="detail_fourth_col"> Question Text</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { 
                                            this.state.exerciseDetails && this.state.exerciseDetails.map((subitem, i) => {
                                                return (
                                                    <tr key = {subitem.key} id={subitem.key}> 
                                                        <td>{subitem.exercise_id}</td>                                                       
                                                        <td> {subitem.label} </td>   
                                                        <td> {subitem.key}</td>   
                                                        <td class = "sub_table" key = {subitem.key} id={subitem.key}>  <div key = {subitem.key} id={subitem.key} 
                                                                dangerouslySetInnerHTML={{
                                                                    __html: subitem.text 
                                                                }}></div> 
                                                            
                                                        </td>
                                                        
                                                    </tr>
                                                )
                                            })
                                        }
                                        <tr>
                                            <td colSpan="4" class="check_answer">
                                            <button id="submit" className="btn btn-primary" value="Submit" disabled={this.state.results.length > 0 && flag == false}  onClick={this.checkAnswer}>Check Answer</button>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <div className="text-center">
                                    <button id="submit" className="btn btn-primary" value="Submit" disabled={this.state.results.length > 0 && flag == false} onClick={this.confirm}>Submit</button>
                                </div>
                            </form>
                            {flag == true &&
                                <table className = "table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th> Account</th>
                                                <th> Debit</th>
                                                <th> Credit</th>
                                                <th> Answer</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { 
                                                this.state.results.map((result, i) => {
                                                    return (
                                                        <tr key = {i}>
                                                            <td>
                                                                <div>
                                                                    {result.account}
                                                                </div>
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.debit}
                                                                </div>
                                                            
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.credit}
                                                                </div>
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    <span style={result.isaccountCorrect && result.isdebitCorrect && result.iscreditCorrect ? {color: "green", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Correct</span>
                                                                    <span style={!result.isaccountCorrect || !result.isdebitCorrect || !result.iscreditCorrect ? {color: "red", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Incorrect</span>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                }            
                            {flag == false &&
                    
                            <div className = "card-body">
                                <div className = "row" style={this.state.results.length > 0 ? {display: 'block' } : {display: 'none'}}>
                                <table className = "table table-striped table-bordered finalResult">
                                        <thead>
                                        { 
                                            this.state.results.map((result, i) => {
                                                if(i==0){
                                                    return (
                                                        <tr>
                                                            <td colSpan="6">
                                                                <span style={result.result ? {color: "green", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Correct</span>
                                                                <span style={!result.result ? {color: "red", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Incorrect</span>

                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                            })
                                        }
                                        </thead>
                                    </table>
                                    <table className = "table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="12%">Exercise Instance Id</th>
                                                <th width="10%"> Detail Key</th>
                                                <th> Account</th>
                                                <th> Debit</th>
                                                <th> Credit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { 
                                                this.state.results.map((result, i) => {
                                                    return (
                                                        <tr key = {i}> 
                                                            <td>{result.exercise_instance_id}</td>
                                                            <td> {result.question_key}</td>
                                                            <td>
                                                                <div>
                                                                    {result.account}
                                                                    <span style={{float:"right"}}>
                                                                    <span style={result.isaccountCorrect ? {color: "green", display: 'block' } : {display: 'none'}}><Icon.CheckLg color="green" size={20} /></span>
                                                                    <span style={!result.isaccountCorrect ? {color: "red", display: 'block' } : {display: 'none'}}><Icon.XLg color="red" size={20} /></span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.debit}
                                                                    <span style={{float:"right"}}>
                                                                    <span style={result.isdebitCorrect ? {color: "green", display: 'block' } : {display: 'none'}}><Icon.CheckLg color="green" size={20} /></span>
                                                                    <span style={!result.isdebitCorrect ? {color: "red", display: 'block' } : {display: 'none'}}><Icon.XLg color="red" size={20} /></span>
                                                                    </span>
                                                                </div>
                                                            
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.credit}
                                                                    <span style={{float:"right"}}>
                                                                    <span style={result.iscreditCorrect ? {color: "green", display: 'block' } : {display: 'none'}}><Icon.CheckLg color="green" size={20} /></span>
                                                                    <span style={!result.iscreditCorrect ? {color: "red", display: 'block' } : {display: 'none'}}><Icon.XLg color="red" size={20} /></span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                <br/>

                                <div className = "row" style={this.state.results.length > 0 ? {display: 'block' } : {display: 'none'}}>
                                    <table className = "grade_calculate">
                                        <thead>
                                            <tr>
                                                <th style={{"color":"red"}}>Scoring</th>
                                                <th> Total Possible</th>
                                                <th> Number Correct </th>
                                                <th> Grade </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <tr> 
                                                <td>Journal entries</td>
                                                <td>{this.state.journalEntry}</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td> Ledger accounts</td>
                                                <td> 0</td>
                                                <td> 0</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td> {this.state.results.length}</td>
                                                <td> 0</td>
                                                <td> 0 %</td>
                                            </tr>
                                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewExerciseComponent