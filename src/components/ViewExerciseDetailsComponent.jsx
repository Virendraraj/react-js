import React, { Component } from 'react'
import ExerciseService from '../services/ExerciseService'

class ViewExerciseDetailsComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            key: this.props.match.params.key,
            exerciseDetails: {}
        }
    }
    componentDidMount(){
        ExerciseService.getDetailsByKey(this.state.key).then( res => {
            console.log(res.data)
            if (res.data.length > 0) {
                this.setState({exerciseDetails: res.data[0]});   
            }
        })
    }
    exerciseList(){
        this.props.history.push('/exercises');
    }
    render() { 
        return (
            <div>
                <br/>
                <div className = "card col-md-12">
                    <br/>
                    <h3 className = "text-center"> Exercise Detail</h3>
                    <hr/>
                    <div className = "card-body">
                        <div className = "row">
                            <table className = "table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th> Detail Label</th>
                                        <th> Detail Key</th>
                                        <th> Question Text</th>
                                        <th> Question Expression</th>
                                        <th> Answer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr key = {this.state.exerciseDetails.exercise_id}>
                                        <td> {this.state.exerciseDetails.label} </td>   
                                        <td> {this.state.exerciseDetails.key} </td>   
                                        <td> {this.state.exerciseDetails.text} </td>   
                                        <td> {this.state.exerciseDetails.expression} </td>   
                                        <td> {this.state.exerciseDetails.answer} </td>   
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className = "row float-right">
                            <button onClick={ () => this.exerciseList()} className="btn btn-secondary">Back </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewExerciseDetailsComponent